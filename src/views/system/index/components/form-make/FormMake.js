import draggable from 'vuedraggable'
export default {
  name: 'FormMake',
  data () {
    return {
      comData: [
        {
          type: 'base',
          name: '基础组件',
          children: [
            { type: 'input', name: '单行文本' },
            { type: 'textarea', name: '多行文本' },
            { type: 'select', name: '下拉框' },
            { type: 'radio', name: '单选框' },
            { type: 'checkbox', name: '多选框' },
            { type: 'date', name: '日期选择器' },
            { type: 'time', name: '时间选择器' },
            { type: 'text', name: '文字' },
            { type: 'switch', name: '开关' },
            { type: 'rate', name: '评分' },
            { type: 'button', name: '提交表单' },
            { type: 'call', name: '拨打电话' }
          ]
        },
        {
          type: 'senior',
          name: '高级组件',
          children: [
            { type: 'cascader', name: '级联' }
            // { type: 'image', name: '图片' }
          ]
        },
        {
          type: 'others',
          name: '定制组件',
          children: [
            { type: 'name', name: '姓名' },
            { type: 'phone', name: '手机号' },
            { type: 'card', name: '身份证号' },
            { type: 'email', name: '邮箱' },
            { type: 'tally', name: '计数' },
            { type: 'signup', name: '已报名用户' }
            // { type: 'deskjobs', name: '提交后文案' },
            // { type: 'bgcolor', name: '背景色' }
          ]
        }
      ],
      tallyData: [
        { value: '5', label: '提交成功' },
        { value: '4', label: '报名成功' },
        { value: '3', label: '领取优惠' },
        { value: '2', label: '参与活动' },
        { value: '1', label: '预约成功' }
      ],
      signupData: [
        { value: '5', label: '已提交' },
        { value: '4', label: '已报名' },
        { value: '3', label: '已领取' },
        { value: '2', label: '已参加' },
        { value: '1', label: '已预约' }
      ],
      tally: '5',
      signup: '5',
      formData: [],
      current: null,
      currentData: {},
      formAttr: {
        backgroundColor: '#FFFFFF'
      },
      canSubmit: 0
    }
  },
  props: {
    infoForm: {
      type: Array,
      default: () => { return [] }
    },
    type: {
      type: String,
      default: ''
    },
    attr: {
      type: Object,
      default: () => {
        return { backgroundColor: '#FFFFFF', submitMsg: '提交成功' }
      }
    }
  },
  components: {
    draggable
  },
  mounted () {
    if (this.infoForm.length !== 0) {
      this.formData = this.infoForm
    }
  },
  watch: {
    currData: {
      deep: true,
      handler (val) {
        this.formData.forEach(item => {
          if (item.key === val.key) {
            item = val
          }
        })
      }
    },
    tally (val) {
      this.currData.options.calculateType = val
    },
    signup (val) {
      this.currData.options.showType = val
    }
  },
  computed: {
    currData: {
      get () {
        return this.currentData
      },
      set (val) {
        this.currentData = val
      }
    }
  },
  methods: {
    // 拖拽右侧组件，克隆到左侧
    cloneCom (data) {
      return this.formatData(data)
    },
    // 点击右侧组件，克隆到左侧
    cloneNode (data) {
      // 判断一共有几个button按钮，超过一个提示
      for (let i = 0; i < this.formData.length; i++) {
        if (this.formData[i].type === 'button' && data.type === 'button') {
          this.$notify.error({
            title: '提示',
            message: '一个页面只能添加一个提交按钮!'
          })
          return false
        }
      }
      new Promise((resolve, reject) => {
        this.formData.push(this.formatData(data))
        resolve(this.formData)
      }).then(res => {
        let button = { type: 'button', name: '提交表单' }
        res.forEach((item, index) => {
          if (item.type === 'input' ||
          item.type === 'textarea' ||
          item.type === 'name' ||
          item.type === 'phone' ||
          item.type === 'card' ||
          item.type === 'email' ||
          item.type === 'select' ||
          item.type === 'radio' ||
          item.type === 'checkbox' ||
          item.type === 'date' ||
          item.type === 'time' ||
          item.type === 'switch' ||
          item.type === 'rate' ||
          item.type === 'cascader') {
            if (this.canSubmit === 0) {
              this.formData.push(this.formatData(button))
              this.canSubmit++
            }
          }
          if (item.type === 'button') {
            let button = this.formData.splice(index, 1)
            this.formData.push(button[0])
          }
        })
      })
    },
    // 数据处理
    formatData (data) {
      let dom = {
        key: new Date().getTime(),
        type: data.type,
        name: data.name,
        options: {}
      }
      let opt = {
        inline: false,
        defaultValue: '',
        showLabel: true,
        required: false,
        labelWidth: 110
      }
      // 单行文本 || 多行文本
      if (dom.type === 'input' || dom.type === 'textarea') {
        opt.pattern = ''
        opt.placeholder = ''
      }
      // 姓名、手机号、身份证号、电子邮箱
      switch (dom.type) {
        case 'name':
          opt.placeholder = '请输入姓名'
          opt.pattern = '^(?:[\u4e00-\u9fa5·]{2,16})$'
          break
        case 'phone':
          opt.placeholder = '请输入手机号'
          opt.pattern = '^(?:(?:\\+|00)86)?1(?:(?:3[\\d])|(?:4[5-7|9])|(?:5[0-3|5-9])|(?:6[5-7])|(?:7[0-8])|(?:8[\\d])|(?:9[1|8|9]))\\d{8}$'
          break
        case 'card':
          opt.placeholder = '请输入身份证号'
          opt.pattern = '(^\\d{8}(0\\d|10|11|12)([0-2]\\d|30|31)\\d{3}$)|(^\\d{6}(18|19|20)\\d{2}(0\\d|10|11|12)([0-2]\\d|30|31)\\d{3}(\\d|X|x)$)'
          break
        case 'email':
          opt.placeholder = '请输入邮箱'
          opt.pattern = '^[a-zA-Z0-9.!#$%&\'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$'
          break
      }
      // 下拉框
      if (dom.type === 'select') {
        opt.placeholder = ''
        opt.options = [ { value: 'Option 1' }, { value: 'Option 2' } ]
      }
      // 单选框 || 多选框
      if (dom.type === 'radio' || dom.type === 'checkbox') {
        opt.options = [ { value: 'Option 1' }, { value: 'Option 2' } ]
      }
      // 日期 || 时间
      if (dom.type === 'date' || dom.type === 'time') {
        opt.placeholder = ''
      }
      // 文本
      if (dom.type === 'text') {
        opt.defaultValue = '这是一段文字'
        opt.style = { fontSize: 14, color: '#999999' }
      }
      // 开关
      if (dom.type === 'switch') {
        opt.defaultValue = false
      }
      // 评分
      if (dom.type === 'rate') {
        opt.defaultValue = 0
      }
      // 按钮
      if (dom.type === 'button') {
        opt.defaultValue = '提交'
        opt.submitMsg = '提交成功'
        opt.style = { color: '#ffffff', backgroundColor: '#07c160' }
      }
      // 拨打电话
      if (dom.type === 'call') {
        opt.defaultValue = '拨打电话'
        opt.phone = ''
        opt.style = { color: '#ffffff', backgroundColor: '#07c160' }
      }
      // 级联
      if (dom.type === 'cascader') {
        opt.placeholder = ''
        opt.defaultValue = 2
        opt.options = ''
      }
      // // 图片
      // if (dom.type === 'image') {
      //   opt.placeholder = ''
      //   opt.options = ''
      // }
      // 计数
      if (dom.type === 'tally') {
        opt.calculateType = this.tallyData[0].value
      }
      // 已报名用户
      if (dom.type === 'signup') {
        opt.showType = this.signupData[0].value
      }

      dom.options = opt
      this.current = dom.key
      this.currentData = dom
      return dom
    },
    // 根据值获取label
    getLabel (data, val) {
      let obj = {}
      obj = data.find(item => {
        return item.value === val
      })
      return obj.label
    },
    // 点击选择表单项
    selectNode (node) {
      this.current = node.key
      this.currentData = node
    },
    // 删除select选项
    delSelOpt (index) {
      this.currData.options.options.splice(index, 1)
    },
    // 添加select选项
    addSelOpt () {
      this.currData.options.options.push({
        value: '新选项'
      })
    },
    // 删除表单项
    delFormItem (index) {
      this.formData.splice(index, 1)
    },
    // 取消保存
    cancel () {
      this.$emit('cancel')
    },
    isJsonHandle (data) {
      if (!this.isJSON(data)) {
        this.$notify.error({
          title: '提示',
          message: '级联数据不是json格式，请重新输入!'
        })
      }
    },
    // 保存
    submit () {
      let valid = new Promise((resolve, reject) => {
        let count = 0
        this.formData.forEach(item => {
          if (item.type === 'cascader') {
            if (item.options.options === '') {
              count += 1
            }
          }
        })
        resolve(count)
      })
      valid.then(res => {
        if (res > 0) {
          this.$notify.error({
            title: '提示',
            message: '级联数据不能为空!'
          })
          return false
        }
        if (this.$refs.phone && this.$refs.phone.value === '') {
          this.$notify.error({
            title: '提示',
            message: '拨打电话的号码不能为空!'
          })
          return false
        }
        this.$emit('submit', this.formData, this.formAttr, this.type)
      })
    },
    isJSON (str) {
      if (typeof str === 'string') {
        try {
          let obj = JSON.parse(str)
          if (typeof obj === 'object' && obj) {
            return true
          } else {
            return false
          }
        } catch (e) {
          console.log('error：' + str + '!!!' + e)
          return false
        }
      }
    }
  }
}
