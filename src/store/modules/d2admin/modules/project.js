import {
  ProjectList,
  AddProject,
  UptProState,
  InfoProject,
  UploadPic,
  DownloadSun,
  DownloadQr,
  ReplacePic,
  PreviewCode,
  ReportPro,
  InfoHot,
  SaveHot,
  RemoveHot,
  InfoShare,
  SaveShare,
  DataAnalysis,
  SaveForm,
  UpdateForm,
  DelProject,
  EditProject,
  GetVersionCount,
  GetOssToken
} from '@api/sys.project'

export default {
  namespaced: true,
  actions: {
    /**
     * @description 查询项目列表
     * @param {Object} context
     * @param {Object} payload page {String} 页码
     * @param {Object} payload limit {String} 每页大小
     * @param {Object} payload t {int} 时间戳
     */
    queryProject ({ dispatch }, {
      page = '',
      limit = '',
      t = ''
    } = {}) {
      return new Promise((resolve, reject) => {
        ProjectList({
          page,
          limit,
          t
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 新增项目
     * @param {Object} context
     * @param {Object} payload name {String} 项目名
     * @param {Object} payload appId {String} 小程序appId
     * @param {Object} payload appSecret {string} 小程序appSecret
     */
    addProject ({ dispatch }, {
      name = '',
      appId = '',
      appSecret = ''
    }) {
      return new Promise((resolve, reject) => {
        AddProject({
          name,
          appId,
          appSecret
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 编辑项目
     * @param {Object} context
     * @param {Object} payload id {String} 项目id
     * @param {Object} payload name {String} 项目名
     * @param {Object} payload appId {String} 小程序appId
     * @param {Object} payload appSecret {string} 小程序appSecret
     */
    editProject ({ dispatch }, {
      id = '',
      name = '',
      appId = '',
      appSecret = ''
    }) {
      return new Promise((resolve, reject) => {
        EditProject({
          id,
          name,
          appId,
          appSecret
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 修改项目状态
     * @param {Object} context
     * @param {Object} payload id {int} 项目id
     */
    uptProState ({ dispatch }, {
      id = ''
    }) {
      return new Promise((resolve, reject) => {
        UptProState({
          id
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 编辑项目信息查询
     * @param {Object} context
     * @param {Object} payload projectId {int} 项目id
     */
    infoProject ({ dispatch }, {
      projectId = ''
    }) {
      return new Promise((resolve, reject) => {
        InfoProject({
          projectId
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 删除项目
     * @param {Object} context
     * @param {Object} payload projectId {int} 项目id
     */
    delProject ({ dispatch }, {
      id = ''
    }) {
      return new Promise((resolve, reject) => {
        DelProject({
          id
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 上传图片
     * @param {Object} payload fileName {string} 文件名
     * @param {Object} payload fileContent {string} 文件base64
     * @param {Object} payload projectId {string} 项目id
     */
    uploadPic ({ dispatch }, {
      fileName = '',
      fileContent = ''
    }) {
      return new Promise((resolve, reject) => {
        UploadPic({
          fileName,
          fileContent
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 替换图片
     * @param {Object} context
     * @param {Object} payload fileName {string} 文件名
     * @param {Object} payload fileContent {string} 文件base64
     * @param {Object} payload id {string} 图片id
     */
    replacePic ({ dispatch }, {
      fileName = '',
      fileContent = '',
      ossId = ''
    }) {
      return new Promise((resolve, reject) => {
        ReplacePic({
          fileName,
          fileContent,
          ossId
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 下载太阳码
     * @param {Object} context
     * @param {Object} payload id {string} 项目id
     */
    downloadSun ({ dispatch }, {
      id = ''
    }) {
      return new Promise((resolve, reject) => {
        DownloadSun({
          id
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 下载二维码
     * @param {Object} context
     * @param {Object} payload id {string} 项目id
     */
    downloadQr ({ dispatch }, {
      id = ''
    }) {
      return new Promise((resolve, reject) => {
        DownloadQr({
          id
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 热区信息
     * @param {Object} context
     * @param {Object} payload ossId {string} 图片id
     */
    infoHot ({ dispatch }, {
      ossId = ''
    }) {
      return new Promise((resolve, reject) => {
        InfoHot({
          ossId
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 保存热区
     * @param {Object} context
     * @param {Object} payload hotData {array} 数组
     */
    saveHot ({ dispatch }, {
      hotData = []
    }) {
      return new Promise((resolve, reject) => {
        SaveHot({
          hotData
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 删除热区
     * @param {Object} context
     * @param {Object} payload id {string} 热区
     */
    removeHot ({ dispatch }, {
      id = ''
    }) {
      return new Promise((resolve, reject) => {
        RemoveHot({
          id
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 预览
     * @param {Object} context
     * @param {Object} payload projectId {string} 项目id
     * @param {Object} payload itemData {array} 图片list
     * @param {Object} payload hotData {array} 热区list
     */
    previewCode ({ dispatch }, {
      projectId = '',
      itemData = [],
      hotData = []
    }) {
      return new Promise((resolve, reject) => {
        PreviewCode({
          projectId,
          itemData,
          hotData
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 发布
     * @param {Object} context
     * @param {Object} payload projectId {string} 项目id
     * @param {Object} payload itemData {array} 图片list
     * @param {Object} payload hotData {array} 热区list
     */
    reportPro ({ dispatch }, {
      projectId = '',
      itemData = [],
      hotData = []
    }) {
      return new Promise((resolve, reject) => {
        ReportPro({
          projectId,
          itemData,
          hotData
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 查看项目分享设置
     * @param {Object} context
     * @param {Object} payload id {string} 项目id
     */
    infoShare ({ dispatch }, {
      id = ''
    }) {
      return new Promise((resolve, reject) => {
        InfoShare({
          id
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 分享设置保存
     * @param {Object} context
     * @param {Object} payload id {string} 项目id
     * @param {Object} payload shareTitle {string} 分享标题
     * @param {Object} payload shareSummary {string} 分享摘要
     * @param {Object} payload sharePicture {string} 照片
     * @param {Object} payload versionState {string} 版本状态
     * @param {Object} payload versionInfo {string} 版本信息
     */
    saveShare ({ dispatch }, {
      id = '',
      shareTitle = '',
      shareSummary = '',
      sharePicture = '',
      versionState = '',
      versionInfo = ''
    }) {
      return new Promise((resolve, reject) => {
        SaveShare({
          id,
          shareTitle,
          shareSummary,
          sharePicture,
          versionState,
          versionInfo
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 数据分析
     * @param {Object} context
     * @param {Object} payload id {string} 项目id
     * @param {Object} payload startDate {string} 开始时间(Y-m-d)
     * @param {Object} payload endDate {string} 结束时间(Y-m-d)
     */
    dataAnalysis ({ dispatch }, {
      id = '',
      startDate = '',
      endDate = ''
    }) {
      return new Promise((resolve, reject) => {
        DataAnalysis({
          id,
          startDate,
          endDate
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 保存表单
     * @param {Object} context
     */
    saveForm ({ dispatch }, {
      formItem = [],
      formName = '',
      formAttr = {}
    }) {
      return new Promise((resolve, reject) => {
        SaveForm({
          formItem,
          formName,
          formAttr
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 更新表单
     * @param {Object} context
     */
    updateForm ({ dispatch }, {
      formItem = [],
      formName = '',
      formId = '',
      formAttr = {}
    }) {
      return new Promise((resolve, reject) => {
        UpdateForm({
          formItem,
          formName,
          formId,
          formAttr
        }).then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 获取修改版权次数
     */
    getCountVersion ({ dispatch }) {
      return new Promise((resolve, reject) => {
        GetVersionCount().then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    },
    /**
     * @description 获取七牛云upToken
     */
    getOssToken ({ dispatch }) {
      return new Promise((resolve, reject) => {
        GetOssToken().then(async res => {
          await resolve(res)
        }).catch(err => {
          console.log('err: ', err)
          reject(err)
        })
      })
    }
  }
}
