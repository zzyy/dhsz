import request from '@/plugin/axios'

// 登录
export function AccountLogin (data) {
  return request({
    url: '/sys/login',
    // url: '/login',
    method: 'post',
    data
  })
}

// 注销登录
export function AccountLogout (data) {
  return request({
    url: '/sys/logout',
    method: 'post',
    data
  })
}

// 修改密码
export function UptPsw (data) {
  return request({
    url: '/sys/user/password',
    method: 'post',
    data
  })
}
