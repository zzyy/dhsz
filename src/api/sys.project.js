import request from '@/plugin/axios'

// 项目列表
export function ProjectList (data) {
  return request({
    url: '/smart/project/list',
    method: 'get',
    data
  })
}
// 新增项目
export function AddProject (data) {
  return request({
    url: '/smart/project/save',
    method: 'post',
    data
  })
}
// 编辑项目
export function EditProject (data) {
  return request({
    url: '/smart/project/updateProject',
    method: 'post',
    data
  })
}
// 项目状态修改
export function UptProState (data) {
  return request({
    url: '/smart/project/update',
    method: 'get',
    data
  })
}
// 编辑项目信息查询
export function InfoProject (data) {
  return request({
    url: '/smart/projectitem/loadProject',
    method: 'get',
    data
  })
}
// 删除项目
export function DelProject (data) {
  return request({
    url: '/smart/project/delete',
    method: 'get',
    data
  })
}
// 上传图片
export function UploadPic (data) {
  return request({
    url: '/smart/projectitem/uploadBase64',
    method: 'post',
    data
  })
}
// 替换图片
export function ReplacePic (data) {
  return request({
    url: '/smart/projectitem/editBase64',
    method: 'post',
    data
  })
}
// 查询热区
export function InfoHot (data) {
  return request({
    url: '/smart/projecthot/info',
    method: 'get',
    data
  })
}
// 保存热区
export function SaveHot (data) {
  return request({
    url: '/smart/projecthot/save',
    method: 'post',
    data
  })
}
// 删除热区
export function RemoveHot (data) {
  return request({
    url: '/smart/projecthot/delete',
    method: 'get',
    data
  })
}
// 太阳码下载
export function DownloadSun (data) {
  return request({
    url: '/smart/project/downloadSunBase64',
    method: 'get',
    data
  })
}
// 二维码下载
export function DownloadQr (data) {
  return request({
    url: '/smart/project/downloadQRBase64',
    method: 'get',
    data
  })
}
// h5页面预览二维码
export function PreviewCode (data) {
  return request({
    url: '/smart/project/tempPreview',
    method: 'post',
    data
  })
}
// 发布
export function ReportPro (data) {
  return request({
    url: '/smart/project/report',
    method: 'post',
    data
  })
}
// 查看项目分享设置
export function InfoShare (data) {
  return request({
    url: '/smart/project/info',
    method: 'get',
    data
  })
}
// 分享设置保存
export function SaveShare (data) {
  return request({
    url: '/smart/project/saveShare',
    method: 'post',
    data
  })
}
// 数据分析
export function DataAnalysis (data) {
  return request({
    url: '/smart/project/dataAnalysis',
    method: 'get',
    data
  })
}
// 视频上传
/**
 * @return {string}
 */
export function UploadVideo () {
  return '/smart/projectitem/uploadVideo'
}
// 替换视频
/**
 * @return {string}
 */
export function UpdateVideo () {
  return '/smart/projectitem/updateVideo'
}
// 保存表单
export function SaveForm (data) {
  return request({
    url: '/smart/projectForm/save',
    method: 'post',
    data
  })
}
// 更新表单
export function UpdateForm (data) {
  return request({
    url: '/smart/projectForm/update',
    method: 'post',
    data
  })
}
// 下载表单数据
/**
 * @return {string}
 */
export const DownloadFormData = '/app/project/export?projectId='
// 邀请地址获取
/**
 * @return {string}
 */
export function GetInviteUrl () {
  return '/sys/user/inviteUrl'
}
// 获取用户信息
export function GetUserInfo () {
  return '/sys/user/info'
}
// 获取修改版权次数
export function GetVersionCount (data) {
  return request({
    url: '/sys/user/getModiflyVersionCount',
    method: 'get',
    data
  })
}
// 获取七牛云upToken
export function GetOssToken (data) {
  return request({
    url: '/sys/oss/getOssToken',
    method: 'get',
    data
  })
}
